// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyAbrG600yNU2AE70deHEJkDjhp5J869cH4",
    authDomain: "fir-example-with-angular-c5c8f.firebaseapp.com",
    databaseURL: "https://fir-example-with-angular-c5c8f.firebaseio.com",
    projectId: "fir-example-with-angular-c5c8f",
    storageBucket: "fir-example-with-angular-c5c8f.appspot.com",
    messagingSenderId: "286846547881",
    appId: "1:286846547881:web:3c42decbe693c3a95eb6b8"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
