# To run this

1. Make sure you are in the correct folder: "FirebaseNoteApp"
2. Run the command "npm i" to install all the dependencies.
3. Run the command "ng serve --o" (it will open it the project on a window)